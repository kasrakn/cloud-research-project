package GA;

import org.cloudbus.cloudsim.Vm;

import java.util.ArrayList;

public class Chromosomes {
    protected ArrayList<Gene> geneList;

    public Chromosomes(ArrayList<Gene> geneList) {
        this.geneList = geneList;
    }

    public ArrayList<Gene> getGeneList() {
        return this.geneList;
    }

}
