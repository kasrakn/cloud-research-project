package GA;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Vm;

//done
public class Gene {
    private final Task task;
    private final int vmIndex;
    private Vm vmType;

    public Gene(Task cl, Vm v, int vmIndex) {
        this.task = cl;
        this.vmType = v;
        this.vmIndex = vmIndex;
    }

    public int getVmIndex() {
        return vmIndex;
    }

    public Task getCloudletFromGene() {
        return this.task;
    }

    public Vm getVmFromGene() {
        return this.vmType;
    }

    public void setVmForGene(Vm vm) {
        this.vmType = vm;
    }
}
