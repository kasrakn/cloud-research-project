package GA;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.UtilizationModelFull;

import java.util.ArrayList;
import java.util.List;

public class Task extends Cloudlet {
    private final List<Task> Left;
    private final List<Task> Right;
    private final double executionTime;
    private int workflowId;

    public Task(int cloudletId, long cloudletLength, int pesNumber, int ram, long cloudletFileSize, long cloudletOutputSize) {
        super(cloudletId, cloudletLength, pesNumber, cloudletFileSize, cloudletOutputSize, new UtilizationModelFull(), new UtilizationModelFull(), new UtilizationModelFull(), false);
        Left = new ArrayList<>();
        Right = new ArrayList<>();
        workflowId = -1;
        this.executionTime = calcuteExecutionTime(pesNumber, ram, cloudletLength, cloudletFileSize);
    }

    public List<Task> getLeft() {
        return Left;
    }

    public void addRightTask(Task task) {
        Right.add(task);
    }

    public void addLeftTask(Task task) {
        Left.add(task);
    }


    public int getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(int id) {
        this.workflowId = id;
    }

    public double getExecutionTime() {
        return executionTime;
    }


    private double calcuteExecutionTime(int cpu, int ram, long mips, long inputData) {
        return (mips*1.0 / (1000L * cpu)) + (inputData*1.0 / ram);
    }

    @Override
    public String toString() {
        return "parent :" + super.toString() +
                " Task{" +
                "taskID=" + this.getCloudletId() +
                ", Right=" + Right +
                ", workflowId=" + workflowId +
                '}';
    }

}
