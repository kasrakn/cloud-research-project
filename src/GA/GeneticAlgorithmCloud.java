package GA;


import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSchedulerSpaceShared;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;

import java.text.DecimalFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static GA.WorkFlow.getPath;

/**
 * An example showing how to create scalable simulations.
 */
public class GeneticAlgorithmCloud {

    private static final int GARepite = 20;
    private static final int GASELECT = 10;
    private static final int GAPOPSize = 20;
    private static final int GAMutationSize = 4;
    /**
     * The cloudlet list.
     */
    private static List<Task> cloudletList;
    /**
     * The vmlist.
     */
    private static List<Vm> vmlist;

    private static List<Vm> createVM(int userId, int vms) {

        // Creates a container to store VMs. This list is passed to the broker
        // later
        LinkedList<Vm> list = new LinkedList<Vm>();

        // VM Parameters
        long size = 10000; // image size (MB)
        int ram = 512; // vm memory (MB)
        int mips = 500;
        long bw = 10;
        int pesNumber = 4;// number of cpus
        String vmm = "Xen"; // VMM name
        Random rOb = new Random();
        // create VMs
        Vm[] vm = new Vm[vms];
        for (int i = 0; i < vms; i++) {
            vm[i] = new Vm(i, userId, mips + rOb.nextInt(500), pesNumber, ram,
                    bw, size, vmm,
                    new CloudletSchedulerSpaceShared());
            // for creating a VM with a space shared scheduling policy for
            // cloudlets:
            // vm[i] = Vm(i, userId, mips, pesNumber, ram, bw, size, priority,
            // vmm, new CloudletSchedulerSpaceShared());
            list.add(vm[i]);
        }

        return list;
    }

    private static List<Task> createTasks() {
        // Creates a container to store Cloudlets
        LinkedList<Task> list = new LinkedList<>();

        return list;
    }

    // //////////////////////// STATIC METHODS ///////////////////////

    /**
     * Creates main() to run this example
     *
     * @param args
     */
    public static void main(String[] args) {
        Log.printLine("Starting GeneticAlgorithmCloudSimulation...");

        try {
            // First step: Initialize the CloudSim package. It should be called
            // before creating any entities.
            int num_user = 1; // number of grid users
            Calendar calendar = Calendar.getInstance();
            boolean trace_flag = false; // mean trace events

            // Initialize the CloudSim library
            CloudSim.init(num_user, calendar, trace_flag);


            // Third step: Create Broker
            DatacenterBroker1 broker = createBroker();
            int brokerId = broker.getId();

            // Fourth step: Create VMs and Cloudlets and send them to broker
            vmlist = createVM(brokerId, 25); // creating 20 vms
            cloudletList = createTasks();

            ArrayList<Chromosomes> initialPopulation = new ArrayList<>();

            int populationSize = initialPopulation.size();
            Random random = new Random();
            for (int itr = 0; itr < GARepite; itr++)// START GA ALGORITHM
            {
                // SELECT CELL FOR mutilation
                List<Pair<Integer,Integer>> selectedParent=new ArrayList<>();
                for (int i = 0; i < GASELECT; i++) {
                    int index1, index2;
                    index1 = random.nextInt(populationSize) % populationSize;
                    index2 = random.nextInt(populationSize) % populationSize;
                    selectedParent.add(new Pair<>(index1, index2));
                }
                //CROSSOVER (index1,index2)
                // CrossoverIndex
                List<Chromosomes> children=new ArrayList<>();
                for (int i = 0; i < selectedParent.size(); i++) {
                    int index1, index2;
                    index1 = selectedParent.get(i).getFirst();
                    index2 = selectedParent.get(i).getSecond();
                    //CROSSOVERINDEX
                    int points = random.nextInt(initialPopulation.get(0).getGeneList().size());
                    ArrayList<Gene> l1 = initialPopulation.get(index1).getGeneList();
                    ArrayList<Gene> l2 = initialPopulation.get(index2).getGeneList();
                    ArrayList<Gene> children1 = new ArrayList<>(l1.subList(0, points));
                    ArrayList<Gene> children2 = new ArrayList<>(l2.subList(0, points));
                    for (Gene gene : l1) {
                        if (!children2.contains(gene)) {
                            children2.add(gene);
                        }
                    }
                    for (Gene gene : l2) {
                        if (!children1.contains(gene)) {
                            children1.add(gene);
                        }
                    }
                    //CROSSOVERVM
                    points = random.nextInt(initialPopulation.get(0).getGeneList().size());
                    for (int j = 0; j < points; j++) {
                        final int v=l1.get(i).getVmIndex();
                        List<Vm> tp1 = l1.stream().filter(item -> item.getVmIndex() == v).map(Gene::getVmFromGene).collect(Collectors.toList());
                        List<Vm> tp2 = l2.stream().filter(item -> item.getVmIndex() == v).map(Gene::getVmFromGene).collect(Collectors.toList());
                        List<Vm> vinX = l1.stream().skip(points).filter(item -> item.getVmIndex() == v).map(Gene::getVmFromGene).collect(Collectors.toList());
                        List<Vm> vinY = l2.stream().skip(points).filter(item -> item.getVmIndex() == v).map(Gene::getVmFromGene).collect(Collectors.toList());
                        if (!tp1.containsAll(tp2) || !tp2.containsAll(tp1)){
                            if (vinX.size()>vinY.size()){
                                l2= (ArrayList<Gene>) l2.stream().map(new Function<Gene, Gene>() {
                                    @Override
                                    public Gene apply(Gene gene) {
                                        if (gene.getVmIndex()==v){
                                            gene.setVmForGene(tp1.get(0));
                                        }
                                        return gene;
                                    }
                                }).collect(Collectors.toList());
                            }else{
                                l1= (ArrayList<Gene>) l1.stream().map(new Function<Gene, Gene>() {
                                    @Override
                                    public Gene apply(Gene gene) {
                                        if (gene.getVmIndex()==v){
                                            if (tp2.size()>0)
                                                gene.setVmForGene(tp2.get(0));
                                        }
                                        return gene;
                                    }
                                }).collect(Collectors.toList());
                            }
                        }
                        final int v2=l2.get(i).getVmIndex();
                        List<Vm> tp21 = l2.stream().filter(item -> item.getVmIndex() == v2).map(Gene::getVmFromGene).collect(Collectors.toList());
                        List<Vm> tp22 = l1.stream().filter(item -> item.getVmIndex() == v2).map(Gene::getVmFromGene).collect(Collectors.toList());
                        List<Vm> vin2X = l1.stream().skip(points).filter(item -> item.getVmIndex() == v2).map(Gene::getVmFromGene).collect(Collectors.toList());
                        List<Vm> vin2Y = l2.stream().skip(points).filter(item -> item.getVmIndex() == v2).map(Gene::getVmFromGene).collect(Collectors.toList());
                        if (!tp21.containsAll(tp22) || !tp22.containsAll(tp21)){
                            if (vin2X.size()>vin2Y.size()){
                                l2= (ArrayList<Gene>) l2.stream().map(gene -> {
                                    if (gene.getVmIndex()==v2){
                                        gene.setVmForGene(tp21.get(0));
                                    }
                                    return gene;
                                }).collect(Collectors.toList());
                            }else{
                                l1= (ArrayList<Gene>) l1.stream().map(new Function<Gene, Gene>() {
                                    @Override
                                    public Gene apply(Gene gene) {
                                        if (gene.getVmIndex()==v){
                                            if (tp22.size()>0)
                                                gene.setVmForGene(tp22.get(0));
                                        }
                                        return gene;
                                    }
                                }).collect(Collectors.toList());
                            }
                        }
                    }
                    Chromosomes chromosome1 = new Chromosomes(l1);
                    Chromosomes chromosome2 = new Chromosomes(l2);
                    children.add(chromosome1);
                    children.add(chromosome2);
                }
                // now time to mutation
                for (int i = 0; i < GAMutationSize; i++) {
                    int points = random.nextInt(initialPopulation.get(0).getGeneList().size());
                    Chromosomes child = children.get(random.nextInt(children.size()));
                    int count=0;
                    for (int j = 1; j < initialPopulation.get(0).getGeneList().size(); j++) {
                        List<Integer> path1 = getPath(child.getGeneList(), 0, j);
                        List<Integer> path2 = getPath(child.getGeneList(), 0, points);
                        if (!path1.containsAll(path2) ||!path2.containsAll(path1)){
                            count++;
                        }
                    }
                }
            }

            ArrayList<Gene> result = initialPopulation.get(0).getGeneList();

            List<Task> finalcloudletList = new ArrayList<>();
            List<Vm> finalvmlist = new ArrayList<>();


            for (Gene gene : result) {
                finalcloudletList.add(gene.getCloudletFromGene());
                finalvmlist.add(gene.getVmFromGene());
                Vm vm = gene.getVmFromGene();
                //Log.printLine("############### VM FROM GENE  "+vm.getId());
            }

            broker.submitVmList(finalvmlist);
            broker.submitCloudletList(finalcloudletList);

            // Fifth step: Starts the simulation
            CloudSim.startSimulation();

            // Final step: Print results when simulation is over
            List<Cloudlet> newList = broker.getCloudletReceivedList();

            CloudSim.stopSimulation();

            printCloudletList(newList);

        } catch (Exception e) {
            e.printStackTrace();
            Log.printLine("The simulation has been terminated due to an unexpected error");
        }
    }


    // We strongly encourage users to develop their own broker policies, to
    // submit vms and cloudlets according
    // to the specific rules of the simulated scenario
    private static DatacenterBroker1 createBroker() {

        DatacenterBroker1 broker = null;
        try {
            broker = new DatacenterBroker1("Broker");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return broker;
    }

    /**
     * Prints the Cloudlet objects
     *
     * @param list list of Cloudlets
     */
    private static void printCloudletList(List<Cloudlet> list) {
        Cloudlet cloudlet;

        String indent = "    ";
        Log.printLine();
        Log.printLine("========== OUTPUT ==========");
        Log.printLine("Cloudlet ID" + indent + "STATUS" + indent
                + "Data center ID" + indent + "VM ID" + indent + indent
                + "Time" + indent + "Start Time" + indent + "Finish Time");

        DecimalFormat dft = new DecimalFormat("###.##");
        for (Cloudlet value : list) {
            cloudlet = value;
            Log.print(indent + cloudlet.getCloudletId() + indent + indent);

            if (cloudlet.getCloudletStatus() == Cloudlet.SUCCESS) {
                Log.print("SUCCESS");

                Log.printLine(indent + indent + cloudlet.getResourceId()
                        + indent + indent + indent + cloudlet.getVmId()
                        + indent + indent + indent
                        + dft.format(cloudlet.getActualCPUTime()) + indent
                        + indent + dft.format(cloudlet.getExecStartTime())
                        + indent + indent + indent
                        + dft.format(cloudlet.getFinishTime()));
            }
        }

    }
}
