package GA;

import java.util.*;

public class WorkFlow {
    public static boolean isTopologySort(List<Task> tasks) {
        Set<Task> done = new HashSet<>();
        for (int i = 0; i < tasks.size(); i++) {
            //before job
            Task currentJob = tasks.get(i);
            for (int i1 = 0; i1 < currentJob.getLeft().size(); i1++) {
                if (!done.contains(currentJob.getLeft().get(i1))) {
                    return false;
                }
            }
            done.add(currentJob);
        }
        return true;
    }

    public static List<Task> makeTopologySort(List<Task> tasks) {
        List<Task> topologySort = new ArrayList<>();

        return topologySort;
    }

    public static List<Integer> getPath(List<Gene> tasks, int start, int end) {
        List<Integer> paths = new ArrayList<>();
        Optional<Task> startPoint = tasks.stream().map(Gene::getCloudletFromGene).filter(task -> task.getCloudletId() == start).findFirst();
        Optional<Task> endPoint = tasks.stream().map(Gene::getCloudletFromGene).filter(task -> task.getCloudletId() == end).findFirst();
        return paths;
    }
}
