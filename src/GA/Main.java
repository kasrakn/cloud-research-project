package GA;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

public class Main {
    public static final int MAX_SIZE = 1024;
    public static final Task startJob = new Task(0, 0, 0, 0, 0, 0);
    public static final Task endJob = new Task(MAX_SIZE, 0, 0, 0, 0, 0);

    public static void main(String[] args) throws IOException {

        ArrayList<Task> tasks1 = getTasks(1);
        addDummyTasks(tasks1);
        tasks1.sort(new TaskComparetor());

        ArrayList<Task> tasks2 = getTasks(2);
        addDummyTasks(tasks2);
        tasks2.sort(new TaskComparetor());

        assignEdges(1, tasks1);
        assignEdges(2, tasks2);


    }

    private static void addDummyTasks(ArrayList<Task> tasksList) {
        startJob.addRightTask(tasksList.get(0));
        tasksList.add(0, startJob);
        endJob.addLeftTask(tasksList.get(tasksList.size() - 1));
        tasksList.add(tasksList.size() + 1, endJob);
    }

    public static void initialization(int m, ArrayList<Integer> taskIndex, ArrayList<Integer> taskVM, ArrayList<Integer> vmType){
        randInit(1, m, taskIndex, taskVM, vmType);
//      mark = [0, 0, ..,0]
    }

    public static void randInit(int i, int m, ArrayList<Integer> taskIndex, ArrayList<Integer> taskVM, ArrayList<Integer> vmType) {
        Random random = new Random();

        if (i == 1) {
            taskIndex.add(0, 1);
            // mark = [0,0, ..., 0]
        } else if (i < m) {
//            int j = randChoose(k | Mark[k] = 0);
//            mark[j] = 1;
            taskIndex.add(i, 10);
            randInit(i + 1, m, taskIndex, taskVM, vmType);
        } else {
//            mark[m] = 1;
//            taskIndex[m] = m;

            for (int x = 0; x < m; x++){
                taskVM.add(x, random.nextInt(m) + 1);
                vmType.add(x, random.nextInt(1) + 1);
            }

            // add vm-index - taskVM - vmType into set of initial population.
        }
    }

    public static ArrayList<Task> getTasks(int workflowID) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("files/Workflow_Task.csv"));
        ArrayList<Task> taskList = new ArrayList<>();

        reader.readLine();
        String line = reader.readLine();

        while (line != null) {
            String[] params = line.split(",");

            int ram = Integer.parseInt(params[5]);
            int cpu = Integer.parseInt(params[6]);
            int workflow = Integer.parseInt(params[0]);
            int taskID = Integer.parseInt(params[1]);
            long mips = Long.parseLong(params[2]);
            long inputData = Long.parseLong(params[3]);
            long outputData = Long.parseLong(params[4]);

            if (workflow == workflowID) {
                Task task = new Task(taskID, mips, cpu, ram, inputData, outputData);
                task.setWorkflowId(workflow);

                taskList.add(task);
            }

            line = reader.readLine();
        }
        reader.close();
        return taskList;
    }

    public static void assignEdges(int workflowID, ArrayList<Task> tasks) throws IOException {
        BufferedReader edgeReader = new BufferedReader(new FileReader("files/WorkFlow_Edge.csv"));

        edgeReader.readLine();
        String edgeLine = edgeReader.readLine();

        while (edgeLine != null) {
            String[] edgeParam = edgeLine.split(",");

            int edgeWorkflowID = Integer.parseInt(edgeParam[0]);
            int edgeLeftTaskID = Integer.parseInt(edgeParam[1]);
            int edgeRightTaskID = Integer.parseInt(edgeParam[2]);

            if (edgeWorkflowID == workflowID) {
                tasks.get(edgeLeftTaskID - 1).addRightTask(tasks.get(edgeRightTaskID - 1));
                tasks.get(edgeRightTaskID - 1).addLeftTask(tasks.get(edgeLeftTaskID - 1));
            }
            edgeLine = edgeReader.readLine();
        }
        edgeReader.close();
    }

    public static class TaskComparetor implements Comparator<Task> {
        @Override
        public int compare(Task obj1, Task obj2) {
            return Integer.compare(obj1.getCloudletId(), obj2.getCloudletId());
        }
    }
}
